package org.auxing.webservice;

import javax.jws.soap.SOAPBinding;
import org.auxing.service.TrafStatueService;

@javax.jws.WebService(targetNamespace = "http://webservice.auxing.org/", serviceName = "TrafStatueWebServiceService", portName = "TrafStatueWebServicePort", wsdlLocation = "WEB-INF/wsdl/TrafStatueWebServiceService.wsdl")
@javax.xml.ws.BindingType(value = javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)
@javax.jws.soap.SOAPBinding(style = javax.jws.soap.SOAPBinding.Style.RPC)
public class TrafStatueWebServiceDelegate {

	org.auxing.webservice.TrafStatueWebService trafStatueWebService = new org.auxing.webservice.TrafStatueWebService();

	public String getTrafStatue(String jsonstr) {
		return trafStatueWebService.getTrafStatue(jsonstr);
	}

	public String getPrePicture(String addressID) {
		return trafStatueWebService.getPrePicture(addressID);
	}

	public String getAdvPicture() {
		return trafStatueWebService.getAdvPicture();
	}

	public String getPathInfo() {
		return trafStatueWebService.getPathInfo();
	}

	public String setReportInfo(String jsonstr) {
		return trafStatueWebService.setReportInfo(jsonstr);
	}

	public String setReportImage(String jsonstr) {
		return trafStatueWebService.setReportImage(jsonstr);
	}

}