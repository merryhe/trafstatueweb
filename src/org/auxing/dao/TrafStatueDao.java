/**
 * 
 */
package org.auxing.dao;

import java.util.List;

import org.auxing.dto.CustLanDto;
import org.auxing.dto.PathInfo;
import org.auxing.dto.TrafStatObj;
import org.auxing.dto.UserDto;

/**
 * 《智行镇江》数据DB交互的接口
 * 
 * @author gaosm
 * 
 */
public interface TrafStatueDao {

	// **********************Android method***********************
	/**
	 * 管理员发布的拥堵列表获取
	 * 
	 * @param addIdList
	 *            json格式的字符串
	 * @return TrafStatObj
	 */
	public List<TrafStatObj> getTrafStatue(List<String> addIdList);

	/**
	 * 手机用户发布的拥堵列表获取
	 * 
	 * @return TrafStatObj
	 */
	public List<TrafStatObj> getReportInfo();

	/**
	 * 预播放图片获取
	 * 
	 * @param addressID
	 *            具体地址
	 * @return TrafStatObj(urlPrePic;errorId)
	 */
	public String getPrePicture(String addressID);

	/**
	 * 广告图片获取
	 * 
	 * @return TrafStatObj(urlAdv;errorId)
	 */
	public String getAdvPicture();

	/**
	 * 路况资讯获取
	 * 
	 * @return TrafStatObj(statue;statueDesc;content;time;address;errorId)
	 */
	public List<PathInfo> getPathInfo();

	/**
	 * 路况上报
	 * 
	 * @param trafStatObj
	 *            Soap消息体
	 * @return 插入数据条数
	 */
	public int insReportInfo(TrafStatObj trafStatObj);
	
	
	// ********************页面method*********************
	/**
	 * 用户信息注册
	 * 
	 * @param userDto
	 *            用户信息
	 */
	public int insUserInfo(UserDto userDto);
	
	/**
	 * 用户信息取得
	 * 
	 * @param userDto
	 *            用户信息
	 * @return
	 */
	public List<UserDto> selUserInfo(UserDto userDto);
	

	/**
	 * 用户信息取得
	 * 
	 * @param userDto
	 *            用户信息
	 * @return
	 */
	public List<UserDto> selUser();
	
	
	/**
	 * 用户删除
	 * 
	 * @param userDtoList
	 * @return
	 */
	public int delUserInfo(UserDto userDtoList);
	
	/**
	 * 用户编辑 
	 * 
	 * @param userDtoList
	 * @return
	 */
	public int updUserInfo(UserDto userDtoList);
	
	/**
	 * 清除密码 
	 * 
	 * @param userDtoList
	 * @return
	 */
	public int updUserPass(List<UserDto> userDtoList);
	
	/**
	 * 常用语检索左边的menu
	 * 
	 * @return
	 */
	public List<CustLanDto> selCustLan();
	
	/**
	 * 常用语检索带参数
	 * 
	 * @return
	 */
	public List<CustLanDto> selCustLan(String custDescribe);

	/**
	 * 常用语更新
	 * 
	 * @param custLanDtoList
	 */
	public void updCustLan(CustLanDto custLanDtoList);
	
	/**
	 * 常用语登陆
	 * 
	 * @param custLanDto
	 */
	public void insCustLan(CustLanDto custLanDto);

	/**
	 * 常用语删除
	 * 
	 * @param custLanDto
	 */
	public void delCustLan(List<CustLanDto> custLanDto);
	
}
