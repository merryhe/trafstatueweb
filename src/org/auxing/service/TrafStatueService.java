package org.auxing.service;

import java.util.List;

import org.auxing.dto.RtnTrafStatueDto;
import org.auxing.logic.TrafStatueLogic;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 《智行镇江》数据交互的service
 * 
 * @author gaosm
 * 
 */
public class TrafStatueService {

	/**
	 * 管理员发布的拥堵列表获取数据的方法
	 * 
	 * @param jsonstr
	 *            json格式的字符串
	 * @return json格式的字符串
	 */
	public String getTrafStatue(String jsonstr) {
		return new TrafStatueLogic().getTrafStatue(jsonstr);
	}

//	/**
//	 * 手机用户发布的拥堵列表获取数据的方法
//	 * 
//	 * @return json格式的字符串
//	 */
//	public String getReportInfo() {
//		return new TrafStatueLogic().getReportInfo();
//	}

	/**
	 * 预播放图片获取的方法
	 * 
	 * @param addressID
	 *            具体地址
	 * @return json格式的字符串
	 */
	public String getPrePicture(String addressID) {
		return new TrafStatueLogic().getPrePicture(addressID);
	}

	/**
	 * 广告图片获取的方法
	 * 
	 * @return json格式的字符串
	 */
	public String getAdvPicture() {
		return new TrafStatueLogic().getAdvPicture();
	}

	/**
	 * 路况资讯获取的方法
	 * 
	 * @return json格式的字符串
	 */
	public String getPathInfo() {
		return new TrafStatueLogic().getPathInfo();
	}

	/**
	 * 路况上报的方法
	 * 
	 * @param jsonstr
	 *            json格式的字符串
	 * @return json格式的字符串
	 */
	public String setReportInfo(String jsonstr) {
		System.out.println("1111111111111111");
		return new TrafStatueLogic().setReportInfo(jsonstr);
	}

	/**
	 * 图片上传
	 * 
	 * @param jsonstr
	 * @return errorId
	 */
	public String setReportImage(String jsonstr) {
		return new TrafStatueLogic().setReportImage(jsonstr);
	}

}
