package org.auxing.dto;

/**
 * Soap消息体
 * 
 * @author Administrator
 * 
 */
public class TrafStatObj {

	// 管理员发布的同statueDesc，普通用户发布的不同于statueDesc为statueDesc+手机串号
	private String addressID;

	// 拥堵/正常/缓刑 value[0/1/2]
	private String statue;

	// 交通点位
	private String statueDesc;

	// 管理员/手机用户 value [“0”/”1”]
	private String user;

	// 内容描述
	private String content;

	// 刷新时间
	private String time;

	// 对应的视频存放地址
	private String url;

	// 对应的图片存放地址
	private String pic_url;

	// 用户是否收藏 [0/1]
	private String isStore;

	// 具体到XX路
	private String address;
	
	// 延迟时间
	private String delayTime;
	
	// 是否屏蔽，0：不屏蔽，1：屏蔽
	private String isShield;

	/**
	 * @return the addressID
	 */
	public String getAddressID() {
		return addressID;
	}

	/**
	 * @param addressID
	 *            the addressID to set
	 */
	public void setAddressID(String addressID) {
		this.addressID = addressID;
	}

	/**
	 * @return the statue
	 */
	public String getStatue() {
		return statue;
	}

	/**
	 * @param statue
	 *            the statue to set
	 */
	public void setStatue(String statue) {
		this.statue = statue;
	}

	/**
	 * @return the statueDesc
	 */
	public String getStatueDesc() {
		return statueDesc;
	}

	/**
	 * @param statueDesc
	 *            the statueDesc to set
	 */
	public void setStatueDesc(String statueDesc) {
		this.statueDesc = statueDesc;
	}

	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the time
	 */
	public String getTime() {
		return time;
	}

	/**
	 * @param time
	 *            the time to set
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the pic_url
	 */
	public String getPic_url() {
		return pic_url;
	}

	/**
	 * @param picUrl
	 *            the pic_url to set
	 */
	public void setPic_url(String picUrl) {
		pic_url = picUrl;
	}

	/**
	 * @return the isStore
	 */
	public String getIsStore() {
		return isStore;
	}

	/**
	 * @param isStore
	 *            the isStore to set
	 */
	public void setIsStore(String isStore) {
		this.isStore = isStore;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the delayTime
	 */
	public String getDelayTime() {
		return delayTime;
	}

	/**
	 * @param delayTime the delayTime to set
	 */
	public void setDelayTime(String delayTime) {
		this.delayTime = delayTime;
	}

	/**
	 * @return the isShield
	 */
	public String getIsShield() {
		return isShield;
	}

	/**
	 * @param isShield the isShield to set
	 */
	public void setIsShield(String isShield) {
		this.isShield = isShield;
	}
}
