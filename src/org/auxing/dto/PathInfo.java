package org.auxing.dto;

/**
 * 路况资讯获取Dto
 * 
 * @author gaosm
 *
 */
public class PathInfo {

	// 拥堵/正常/缓刑 value[0/1/2]
	private int statue;

	// 交通状况描述
	private String statueDesc;

	// 内容描述
	private String content;

	// 刷新时间
	private String time;

	// 具体到XX路
	private String address;

	/**
	 * @return the statue
	 */
	public int getStatue() {
		return statue;
	}

	/**
	 * @param statue the statue to set
	 */
	public void setStatue(int statue) {
		this.statue = statue;
	}

	/**
	 * @return the statueDesc
	 */
	public String getStatueDesc() {
		return statueDesc;
	}

	/**
	 * @param statueDesc the statueDesc to set
	 */
	public void setStatueDesc(String statueDesc) {
		this.statueDesc = statueDesc;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the time
	 */
	public String getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
}
