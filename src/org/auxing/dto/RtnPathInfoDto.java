package org.auxing.dto;

import java.util.List;

/**
 * 路况资讯获取数据返回用
 * 
 * @author gaosm
 *
 */
public class RtnPathInfoDto {

	// 路况资讯获取数组
	private List<PathInfo> pathInfoList;
	
	//ERROR CODE
	private String errorId;

	/**
	 * @return the pathInfoList
	 */
	public List<PathInfo> getPathInfoList() {
		return pathInfoList;
	}

	/**
	 * @param pathInfoList the pathInfoList to set
	 */
	public void setPathInfoList(List<PathInfo> pathInfoList) {
		this.pathInfoList = pathInfoList;
	}

	/**
	 * @return the errorId
	 */
	public String getErrorId() {
		return errorId;
	}

	/**
	 * @param errorId the errorId to set
	 */
	public void setErrorId(String errorId) {
		this.errorId = errorId;
	}
}
