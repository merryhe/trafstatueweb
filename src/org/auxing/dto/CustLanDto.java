package org.auxing.dto;

/**
 * 常用语信息
 * 
 * @author gaosm
 *
 */
public class CustLanDto {
	
	// 常用语简述
	private String custDescribe;
	
	// 详细信息
	private String information;

	/**
	 * @return the custDescribe
	 */
	public String getCustDescribe() {
		return custDescribe;
	}

	/**
	 * @param custDescribe the custDescribe to set
	 */
	public void setCustDescribe(String custDescribe) {
		this.custDescribe = custDescribe;
	}

	/**
	 * @return the information
	 */
	public String getInformation() {
		return information;
	}

	/**
	 * @param information the information to set
	 */
	public void setInformation(String information) {
		this.information = information;
	}
}
