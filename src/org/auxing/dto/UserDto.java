package org.auxing.dto;

/**
 * 用户信息
 * 
 * @author gaosm
 *
 */
public class UserDto {
	
	// 警员编号
	private String jybh;
	
	// 终端号码
	private String zdhm;
	
	// 用户姓名
	private String yhxm;
	
	// 登陆密码
	private String dlmm;
	
	// 所属区域
	private String ssqy;
	
	// 申请成为管理员
	private String sqgly;

	/**
	 * @return the jybh
	 */
	public String getJybh() {
		return jybh;
	}

	/**
	 * @param jybh the jybh to set
	 */
	public void setJybh(String jybh) {
		this.jybh = jybh;
	}

	/**
	 * @return the zdhm
	 */
	public String getZdhm() {
		return zdhm;
	}

	/**
	 * @param zdhm the zdhm to set
	 */
	public void setZdhm(String zdhm) {
		this.zdhm = zdhm;
	}

	/**
	 * @return the yhxm
	 */
	public String getYhxm() {
		return yhxm;
	}

	/**
	 * @param yhxm the yhxm to set
	 */
	public void setYhxm(String yhxm) {
		this.yhxm = yhxm;
	}

	/**
	 * @return the dlmm
	 */
	public String getDlmm() {
		return dlmm;
	}

	/**
	 * @param dlmm the dlmm to set
	 */
	public void setDlmm(String dlmm) {
		this.dlmm = dlmm;
	}

	/**
	 * @return the ssqy
	 */
	public String getSsqy() {
		return ssqy;
	}

	/**
	 * @param ssqy the ssqy to set
	 */
	public void setSsqy(String ssqy) {
		this.ssqy = ssqy;
	}

	/**
	 * @return the sqgly
	 */
	public String getSqgly() {
		return sqgly;
	}

	/**
	 * @param sqgly the sqgly to set
	 */
	public void setSqgly(String sqgly) {
		this.sqgly = sqgly;
	}

}
