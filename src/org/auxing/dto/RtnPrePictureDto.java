package org.auxing.dto;

/**
 * 预播放图片获取数据返回用
 * 
 * @author gaosm
 *
 */
public class RtnPrePictureDto {
	
	// 预播放图片获取
	private String urlPrePic;
	
	// ERROR CODE
	private String errorId;

	/**
	 * @return the urlPrePic
	 */
	public String getUrlPrePic() {
		return urlPrePic;
	}

	/**
	 * @param urlPrePic the urlPrePic to set
	 */
	public void setUrlPrePic(String urlPrePic) {
		this.urlPrePic = urlPrePic;
	}

	/**
	 * @return the errorId
	 */
	public String getErrorId() {
		return errorId;
	}

	/**
	 * @param errorId the errorId to set
	 */
	public void setErrorId(String errorId) {
		this.errorId = errorId;
	}

}
