package org.auxing.logic;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.auxing.dao.TrafStatueDao;
import org.auxing.dto.UserDto;

/**
 * 页面用method
 * 
 * @author gaosm
 *
 */
public class TrafStatueServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// mybatis接口
	private static TrafStatueDao trafStatueDao;
	
	private RequestDispatcher dispatcher = null;
	
	// 用户名
	private String username = null;
	// 密码
	private String password = null;

	// SqlSession定义
	private static SqlSession sqlSession;
	static SqlSessionFactory sqlSessionFactory = null;
	static {
		sqlSessionFactory = DbInit.getSqlSessionFactory();
	}

	
	public void doGet(HttpServletRequest request, HttpServletResponse response) {

		try {
			// 页面参数取得
			username = request.getParameter("zhanghao");
			password = request.getParameter("mima");
			
			// 数据库检索的参数设定
			UserDto inUserDto = new UserDto();
			inUserDto.setUpdDate(username);
			inUserDto.setDlmm(password);
			
			// 重新建立一个新的session
			sqlSession = sqlSessionFactory.openSession();
			// 配置文件中的namespace
			trafStatueDao = (TrafStatueDao) sqlSession
					.getMapper(TrafStatueDao.class);

			UserDto retUserDto = trafStatueDao.selUserInfo(inUserDto);
			
			if (retUserDto != null) {
				dispatcher = request.getRequestDispatcher("/index.jsp");

				dispatcher .forward(request, response);
			} else {
				dispatcher = request.getRequestDispatcher("/login.jsp");

				dispatcher .forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		doGet(request,response);
	}
		
	/**
	* Initialization of the servlet. <br>
	*
	* @throws ServletException if an error occure
	*/
	public void init() throws ServletException {
	// Put your code here
	}
	
}
