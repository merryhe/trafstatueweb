package org.auxing.logic;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.auxing.dao.TrafStatueDao;
import org.auxing.dto.PathInfo;
import org.auxing.dto.ReportPic;
import org.auxing.dto.RtnAdvPictureDto;
import org.auxing.dto.RtnPathInfoDto;
import org.auxing.dto.RtnPrePictureDto;
import org.auxing.dto.RtnTrafStatueDto;
import org.auxing.dto.TrafStatObj;

import sun.misc.BASE64Decoder;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 《智行镇江》数据交互的logic
 * 
 * @author gaosm
 * 
 */
public class TrafStatueLogic extends DbInit {

	private static TrafStatueDao trafStatueDao;

	// GSON对象定义
	private Gson gson = new Gson();

	// 返回登录验证的结果，0-表示成功 -1表示失败
	private String errorId;

	// DB取得数据对象定义
	private List<TrafStatObj> trafStatObjList = new ArrayList<TrafStatObj>();

	// SqlSession定义
	private static SqlSession sqlSession;
	static SqlSessionFactory sqlSessionFactory = null;
	static {
		sqlSessionFactory = DbInit.getSqlSessionFactory();
	}

	// ******************************Android method***********************
	/**
	 * 拥堵列表获取
	 * 
	 * @param jsonstr
	 *            json格式的字符串
	 * @return TrafStatObj
	 */
	public String getTrafStatue(String jsonstr) {

		// 使用Type类，取得相应类型对象的class属性。
		// TypeToken内的泛型就是Json数据中的类型
		List<String> addIdList = new Gson().fromJson(jsonstr,
				new TypeToken<List<String>>() {
				}.getType());// 使用该class属性，获取的对象均是list类型的

		// 重新建立一个新的session
		sqlSession = sqlSessionFactory.openSession();

		// 配置文件中的namespace
		trafStatueDao = (TrafStatueDao) sqlSession
				.getMapper(TrafStatueDao.class);

		// 入力AddressId时，从数据库取得相关数据
		// 管理员发布的拥堵列表获取
		List<TrafStatObj> trafStatObjList = trafStatueDao.getTrafStatue(addIdList);
		
		// 手机用户发布的拥堵列表获取
		List<TrafStatObj> trafStatObjAndroidList = getReportInfo();
		
		trafStatObjList.addAll(trafStatObjAndroidList);

		if (trafStatObjList.size() > 0) {
			errorId = "0";
		} else {
			errorId = "1";
		}

		// 返回值设置
		RtnTrafStatueDto rtnTrafStatueDto = new RtnTrafStatueDto();
		rtnTrafStatueDto.setRtnTrafStatObjList(trafStatObjList);
		rtnTrafStatueDto.setErrorId(errorId);

		// 管理员发布的拥堵列表获取返回
		return gson.toJson(rtnTrafStatueDto);
	}

	/**
	 * 手机用户发布的拥堵列表获取
	 * 
	 * @return TrafStatObj
	 */
	public List<TrafStatObj> getReportInfo() {

		// 重新建立一个新的session
		sqlSession = sqlSessionFactory.openSession();
		// 配置文件中的namespace
		trafStatueDao = (TrafStatueDao) sqlSession
				.getMapper(TrafStatueDao.class);

		// 从数据库取得相关数据
		trafStatObjList = trafStatueDao.getReportInfo();

		// 手机用户发布的拥堵列表获取返回
		return trafStatObjList;
	}

	/**
	 * 预播放图片获取
	 * 
	 * @param addressID
	 *            具体地址
	 * @return TrafStatObj(urlPrePic;errorId)
	 */
	public String getPrePicture(String addressID) {

		String urlPrePic = null;
		if (addressID != null && addressID != "") {

			// 使用Type类，取得相应类型对象的class属性。
			Type listType = new TypeToken<String>() {
			}.getType(); // TypeToken内的泛型就是Json数据中的类型
			String addId = gson.fromJson(addressID, listType); // 使用该class属性，获取的对象均是list类型的

			// 重新建立一个新的session
			sqlSession = sqlSessionFactory.openSession();
			// 配置文件中的namespace
			trafStatueDao = (TrafStatueDao) sqlSession
					.getMapper(TrafStatueDao.class);

			// 入力AddressId时，从数据库取得相关数据
			urlPrePic = trafStatueDao.getPrePicture(addId);
		}

		if (urlPrePic != null && urlPrePic != "") {
			errorId = "0";
		} else {
			errorId = "1";
		}

		// 返回值设置
		RtnPrePictureDto prePictureDto = new RtnPrePictureDto();
		prePictureDto.setUrlPrePic(urlPrePic);
		prePictureDto.setErrorId(errorId);

		// 预播放图片获取返回
		return gson.toJson(prePictureDto);
	}

	/**
	 * 广告图片获取
	 * 
	 * @return TrafStatObj(urlAdv;errorId)
	 */
	public String getAdvPicture() {

		// 重新建立一个新的session
		sqlSession = sqlSessionFactory.openSession();
		// 配置文件中的namespace
		trafStatueDao = (TrafStatueDao) sqlSession
				.getMapper(TrafStatueDao.class);

		String urlAdv = trafStatueDao.getAdvPicture();

		if (urlAdv != null && urlAdv != "") {
			errorId = "0";
		} else {
			errorId = "1";
		}

		// 返回值设置
		RtnAdvPictureDto advPictureDto = new RtnAdvPictureDto();
		advPictureDto.setAdvAddress(urlAdv);
		advPictureDto.setErrorId(errorId);

		// 广告图片存放地址取得并返回
		return gson.toJson(advPictureDto);
	}

	/**
	 * 路况资讯获取
	 * 
	 * @return TrafStatObj(statue;statueDesc;content;time;address;errorId)
	 */
	public String getPathInfo() {

		// 重新建立一个新的session
		sqlSession = sqlSessionFactory.openSession();
		// 配置文件中的namespace
		trafStatueDao = (TrafStatueDao) sqlSession
				.getMapper(TrafStatueDao.class);

		List<PathInfo> pathInfoList = trafStatueDao.getPathInfo();

		if (pathInfoList.size() > 0) {
			errorId = "0";
		} else {
			errorId = "1";
		}

		// 返回值设置
		RtnPathInfoDto rtnPathInfoDto = new RtnPathInfoDto();
		rtnPathInfoDto.setPathInfoList(pathInfoList);
		rtnPathInfoDto.setErrorId(errorId);

		// 路况资讯获取
		return gson.toJson(rtnPathInfoDto);
	}

	/**
	 * 路况上报
	 * 
	 * @param jsonstr
	 *            json格式的字符串
	 * @return json格式的字符串
	 */
	public String setReportInfo(String jsonstr) {

		int rtn = 0;
		errorId = "0";
		if (jsonstr != null && jsonstr != "") {

			try {
				// 使用Type类，取得相应类型对象的class属性。
				Type listType = new TypeToken<TrafStatObj>() {
				}.getType();// TypeToken内的泛型就是Json数据中的类型
				TrafStatObj trafStatObj = gson.fromJson(jsonstr, listType);// 使用该class属性，获取的对象均是list类型的

				// 重新建立一个新的session
				sqlSession = sqlSessionFactory.openSession();
				// 配置文件中的namespace
				trafStatueDao = (TrafStatueDao) sqlSession
						.getMapper(TrafStatueDao.class);

				// 向数据库登录相关数据
				rtn = trafStatueDao.insReportInfo(trafStatObj);
				sqlSession.commit();
			} catch (Exception e) {
				errorId = "-1";
				e.printStackTrace();
				return gson.toJson(errorId);
			} finally {
				sqlSession.close();
			}
		}

		if (rtn == 0) {
			errorId = "1";
		} else {
			errorId = "0";
		}

		return gson.toJson(errorId);
	}

	/**
	 * 图片上传
	 * 
	 * @param jsonstr
	 * @return
	 */
	public String setReportImage(String jsonstr) {

		// 存储路径
		FileOutputStream fos = null;
		try {
			// 使用Type类，取得相应类型对象的class属性。
			Type listType = new TypeToken<ReportPic>() {
			}.getType();// TypeToken内的泛型就是Json数据中的类型
			ReportPic reportPic = gson.fromJson(jsonstr, listType);// 使用该class属性，获取的对象均是list类型的

			String toDir = "E:\\Workspaces\\MyEclipse 8.5\\TrafStatueWebService\\WebRoot\\imageAndroid"; // 存储路径
			byte[] buffer = new BASE64Decoder().decodeBuffer(reportPic
					.getImage()); // 对android传过来的图片字符串进行解码

			File destDir = new File(toDir);
			if (!destDir.exists()) {
				destDir.mkdir();
			}

			fos = new FileOutputStream(new File(destDir + "\\"
					+ reportPic.getPic_name())); // 保存图片
			fos.write(buffer);
			fos.flush();
			fos.close();
			errorId = "0";
		} catch (Exception e) {
			errorId = "-1";
			e.printStackTrace();
		}
		return gson.toJson(errorId);
	}

	/**
	 * 登陆时间，更新时间格式变换
	 */
	private String timeToStr() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm"); // 设置日期格式
		String date = df.format(new Date()); // new Date()为获取当前系统时间
		return date;
	}
}
