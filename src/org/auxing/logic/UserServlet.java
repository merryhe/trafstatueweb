package org.auxing.logic;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.auxing.dao.TrafStatueDao;
import org.auxing.dto.UserDto;

/**
 * 用户登陆用servlet
 * 
 * @author gaosm
 * 
 */
public class UserServlet extends HttpServlet {

	/**
	 * 1L
	 */
	private static final long serialVersionUID = 1L;

	// mybatis接口
	private static TrafStatueDao trafStatueDao;

	private RequestDispatcher dispatcher = null;

	// 警员编号
	private String jybh = null;

	// 终端号码
	private String zdhm = null;

	// 用户姓名
	private String yhxm = null;

	// 登陆密码
	private String dlmm = null;

	// 所属区域
	private String ssqy = null;

	// 申请成为管理员
	private String sqgly = null;

	// SqlSession定义
	private static SqlSession sqlSession;
	static SqlSessionFactory sqlSessionFactory = null;
	static {
		sqlSessionFactory = DbInit.getSqlSessionFactory();
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String method = request.getParameter("method");
		if ("add".equals(method)) {
			userServletIns(request, response);
		} else if ("sel".equals(method)) {
			userServletSel(request, response);
		} else if ("del".equals(method)) {
			userServletDel(request, response);
		} else if ("upd".equals(method)) {
			userServletUpd(request, response);
		} else if ("passClear".equals(method)) {
			userPassServletUpd(request, response);
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	/**
	 * Initialization of the servlet. <br>
	 * 
	 * @throws ServletException
	 *             if an error occure
	 */
	public void init() throws ServletException {
		// Put your code here
	}

	/**
	 * 用户检索
	 * 
	 * @param request
	 * @param response
	 */
	public List<UserDto> userServletSel(HttpServletRequest request,
			HttpServletResponse response) {

		List<UserDto> retUserDtoList = null;
		try {
			// 页面参数取得
			yhxm = request.getParameter("yhxm");
			dlmm = request.getParameter("dlmm");
			String flag = request.getParameter("flag");

			// 数据库检索的参数设定
			UserDto inUserDto = new UserDto();
			inUserDto.setYhxm(yhxm);
			inUserDto.setDlmm(dlmm);

			// 重新建立一个新的session
			sqlSession = sqlSessionFactory.openSession();
			// 配置文件中的namespace
			trafStatueDao = (TrafStatueDao) sqlSession
					.getMapper(TrafStatueDao.class);

			retUserDtoList = trafStatueDao.selUserInfo(inUserDto);

			if (retUserDtoList.size() > 0) {
				dispatcher = request.getRequestDispatcher("/index.jsp");
				dispatcher.forward(request, response);
			} else {
				if ("0".equals(flag)){
					dispatcher = request.getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);
				} else if("1".equals(flag)) {
					PopAlert.checkAlert(request, response);
					dispatcher = request.getRequestDispatcher("/userinfo.jsp");
					dispatcher.forward(request, response);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retUserDtoList;
	}

	/**
	 * 用户检索
	 * 
	 * @param request
	 * @param response
	 */
	public List<UserDto> userinfoServletSel(HttpServletRequest request,
			HttpServletResponse response) {

		// 重新建立一个新的session
		sqlSession = sqlSessionFactory.openSession();
		// 配置文件中的namespace
		trafStatueDao = (TrafStatueDao) sqlSession
				.getMapper(TrafStatueDao.class);

		List<UserDto> retUserDtoList = trafStatueDao.selUser();

		return retUserDtoList;
	}

	/**
	 * 用户注册
	 * 
	 * @param request
	 * @param response
	 */
	public void userServletIns(HttpServletRequest request,
			HttpServletResponse response) {
		try {
			// 页面参数取得
			jybh = request.getParameter("jybh");
			zdhm = request.getParameter("zdhm");
			yhxm = request.getParameter("yhxm");
			dlmm = request.getParameter("dlmm");
			ssqy = request.getParameter("ssqy");
			sqgly = request.getParameter("sqgly");

			if ("on".equals(sqgly)) {
				sqgly = "1";
			} else {
				sqgly = "0";
			}

			// 数据库检索的参数设定
			UserDto inUserDto = new UserDto();
			inUserDto.setJybh(jybh);
			inUserDto.setZdhm(zdhm);
			inUserDto.setYhxm(yhxm);
			inUserDto.setDlmm(dlmm);
			inUserDto.setSsqy(ssqy);
			inUserDto.setSqgly(sqgly);

			// 重新建立一个新的session
			sqlSession = sqlSessionFactory.openSession();
			// 配置文件中的namespace
			trafStatueDao = (TrafStatueDao) sqlSession
					.getMapper(TrafStatueDao.class);

			int rtn = trafStatueDao.insUserInfo(inUserDto);

			if (rtn > 0) {
				dispatcher = request.getRequestDispatcher("/login.jsp");
				dispatcher.forward(request, response);
			} else {
				PopAlert.regAlert(request, response);
				dispatcher = request.getRequestDispatcher("/register.jsp");
				dispatcher.forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 用户删除
	 * 
	 * @param request
	 * @param response
	 */
	public void userServletDel(HttpServletRequest request,
			HttpServletResponse response) {

		try {
			// 页面参数取得
			jybh = request.getParameter("jybh");
			yhxm = request.getParameter("yhxm");
			ssqy = request.getParameter("ssqy");
			sqgly = request.getParameter("sqgly");
			
			// 数据库检索的参数设定
			UserDto inUserDto = new UserDto();
			inUserDto.setJybh(jybh);
			inUserDto.setYhxm(yhxm);

			// 重新建立一个新的session
			sqlSession = sqlSessionFactory.openSession();
			// 配置文件中的namespace
			trafStatueDao = (TrafStatueDao) sqlSession
					.getMapper(TrafStatueDao.class);

			int rtn = trafStatueDao.delUserInfo(inUserDto);

			if (rtn > 0) {
				dispatcher = request.getRequestDispatcher("/userinfo.jsp");
				dispatcher.forward(request, response);
			} else {
				PopAlert.delAlert(request, response);
				dispatcher = request.getRequestDispatcher("/deluser.jsp");
				dispatcher.forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 用户编辑
	 * 
	 * @param request
	 * @param response
	 */
	public void userServletUpd(HttpServletRequest request,
			HttpServletResponse response) {

		try {
			// 页面参数取得
			jybh = request.getParameter("jybh");
			yhxm = request.getParameter("yhxm");
			ssqy = request.getParameter("ssqy");
			sqgly = request.getParameter("sqgly");
			dlmm = request.getParameter("dlmm");
			zdhm = request.getParameter("zdhm");

			// 数据库检索的参数设定
			UserDto inUserDto = new UserDto();
			inUserDto.setJybh(jybh);
			inUserDto.setYhxm(yhxm);
			inUserDto.setSsqy(ssqy);
			inUserDto.setSqgly(sqgly);
			inUserDto.setZdhm(zdhm);
			inUserDto.setDlmm(dlmm);

			// 重新建立一个新的session
			sqlSession = sqlSessionFactory.openSession();
			// 配置文件中的namespace
			trafStatueDao = (TrafStatueDao) sqlSession
					.getMapper(TrafStatueDao.class);

			trafStatueDao.updUserInfo(inUserDto);

			dispatcher = request.getRequestDispatcher("/userinfo.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			try {
				PopAlert.updAlert(request, response);
				dispatcher = request.getRequestDispatcher("/edituser.jsp");
				dispatcher.forward(request, response);
			} catch (ServletException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
	}

	/**
	 * 密码清除
	 * 
	 * @param request
	 * @param response
	 */
	public void userPassServletUpd(HttpServletRequest request,
			HttpServletResponse response) {

		try {
			// 页面参数取得
			jybh = request.getParameter("jybh");
			yhxm = request.getParameter("yhxm");
			ssqy = request.getParameter("ssqy");
			sqgly = request.getParameter("sqgly");

			List<UserDto> userDtoList = new ArrayList<UserDto>();
			// 数据库检索的参数设定
			UserDto inUserDto = new UserDto();
			inUserDto.setJybh(jybh);
			inUserDto.setYhxm(yhxm);
			inUserDto.setSsqy(ssqy);
			inUserDto.setSqgly(sqgly);
			userDtoList.add(inUserDto);

			// 重新建立一个新的session
			sqlSession = sqlSessionFactory.openSession();
			// 配置文件中的namespace
			trafStatueDao = (TrafStatueDao) sqlSession
					.getMapper(TrafStatueDao.class);

			int rtn = trafStatueDao.updUserPass(userDtoList);

			if (rtn > 0) {
				dispatcher = request.getRequestDispatcher("/index.jsp");
				dispatcher.forward(request, response);
			} else {
				dispatcher = request.getRequestDispatcher("/register.jsp");
				dispatcher.forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
