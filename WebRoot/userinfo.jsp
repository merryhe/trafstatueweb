<%@ page language="java" pageEncoding="utf-8"%>
<%@ page contentType="text/html; charset=utf-8" language="java"%>
<%@ page
	import="java.util.*,org.auxing.dto.UserDto,org.auxing.logic.UserServlet"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>镇江移动视频维护平台</title>
		<base href="<%=basePath%>">

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

		<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #EEF2FB;
}
-->
</style>
		<script type="text/javascript">
			function search_sub()
			{
				window.location.href='UserServlet?method='+"sel"+'&yhxm='+document.getElementById('search').value+'&flag'+"1";
			}
		</script>
	</head>
	<body bgcolor="#EEF2FB">
		<table width="100%" height="31" border="0" cellpadding="0"
			cellspacing="0" class="left_topbg" id="table2">
			<tr>
				<td height="31">
					<div class="titlebt">
						用户管理
					</div>
				</td>
			</tr>
		</table>
		<div style="width: 100%" align="center">
			<div style="width: 90%">
				<div class="sidebar_search">
					<form>
						<input type="text" id="search" name="search" class="search_input" value="请输入用户名"
							onClick="this.value = ''" />
						<input type="image" class="search_submit" src="image/search.png" onclick="search_sub();"/>
					</form>
				</div>
				<br>
				<div align="left">
					<table width="35%" height="31" border="0" cellpadding="0"
						cellspacing="0" class="left_topbg" id="table2">
						<tr>
							<td height="31">
								<div class="titlebt1">
									<a href="adduser.jsp" style="cursor: hand"><img
											src="image/plus.gif" />添加用户</a>
								</div>
							</td>
						</tr>
					</table>
				</div>
				
				<br>
				<div style="text-align: left">
					用户信息一览表
				</div>
				<br>
				<div>
					<table id="rounded-corner"
						summary="2007 Major IT Companies' Profit">
						<thead>
							<tr>
								<th scope="col" class="rounded-company" align="left">
								</th>
								<th scope="col" class="rounded" width="14%" align="left">
									姓名
								</th>
								<th scope="col" class="rounded" width="14%" align="left">
									编号
								</th>
								<th scope="col" class="rounded" width="14%" align="left">
									角色
								</th>
								<th scope="col" class="rounded" width="30%" align="left">
									描述
								</th>
								<th scope="col" class="rounded" width="14%" align="left">
									编辑
								</th>
								<th scope="col" class="rounded-q4" width="14%" align="left">
									删除
								</th>
							</tr>
						</thead>
						<tbody>
							<%
								UserServlet u_servlet = new UserServlet();
								List<UserDto> userDtoList = u_servlet.userinfoServletSel(request,
										response);
								int resultconts = userDtoList.size(); // 取得总的数据数
								int countprepage = 8; // 每页的数据条数
								int totalPages = 0; // 取出总页数
								int pageNum; // 当前页码
								int curUser = 0;
								String str_pageNum = request.getParameter("pageNum");

								if (null == str_pageNum || "" == str_pageNum
										|| !str_pageNum.matches("[0-9]{1,}")) {
									pageNum = 1;

								} else {
									pageNum = Integer.parseInt(str_pageNum);

								}

								totalPages = (resultconts - 1) / countprepage + 1;
								if (pageNum >= totalPages) {
									pageNum = totalPages;
								}

								Iterator<UserDto> it = userDtoList.iterator();
								while (it.hasNext()) {
									curUser++;
									UserDto userDto = it.next();
									if (curUser >= (pageNum - 1) * countprepage + 1
											&& pageNum <= pageNum * countprepage) {
										String jybh = userDto.getJybh(); // 警员编号
										String yhxm = userDto.getYhxm(); // 用户姓名
										String sqgly = userDto.getSqgly(); // 申请成为管理员,角色
										String ssqy = userDto.getSsqy(); // 所属区域,描述
										String dlmm = userDto.getDlmm(); // 登陆密码
										String zdhm = userDto.getZdhm(); // 终端号码
							%>
							<tr>
								<td>
								</td>
								<td>
									<%=yhxm%>
								</td>
								<td>
									<%=jybh%>
								</td>
								<td>
									<%=sqgly%>
								</td>
								<td>
									<%=ssqy%>
								</td>
								<td>
									<a href="edituser.jsp?jybh=<%=jybh%>&yhxm=<%=yhxm%>&sqgly=<%=sqgly%>&ssqy=<%=ssqy%>&dlmm=<%=dlmm%>&zdhm=<%=zdhm%>"><img src="image/user_edit.png" alt="编辑" title=""
											border="0" />更新</a>
								</td>
								<td>
									<a href="deluser.jsp?jybh=<%=jybh%>&yhxm=<%=yhxm%>&sqgly=<%=sqgly%>&ssqy=<%=ssqy%>&dlmm=<%=dlmm%>&zdhm=<%=zdhm%>"><img src="image/trash.png" alt="删除"
											title="" border="0" />删除</a>
								</td>

							</tr>
							<%
								}
								}
							%>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div align="center" class="showpage">
			共
			<b><%=totalPages%></b> 页&nbsp;&nbsp;&nbsp;
			<%
				if (pageNum == 1) {
			%>
			第一页| 上一页 |
			<%
				} else {
			%>
			<a href="userinfo.jsp?pageNum=1">第一页</a> |
			<a href="userinfo.jsp?pageNum=<%=pageNum - 1%>">上一页 </a>|
			<%
				}
				if (pageNum == totalPages) {
			%>
			下一页 |尾页&nbsp;&nbsp;&nbsp;
			<%
				} else {
			%>
			<a href="userinfo.jsp?pageNum=<%=pageNum + 1%>">下一页</a> |
			<a href="userinfo.jsp?pageNum=<%=totalPages%>">尾页 </a>|&nbsp;&nbsp;&nbsp;
			<%
				}
			%>
			转到第
			<select
				onchange="window.location='userinfo.jsp?pageNum='+this.value;">
				<%
					for (int i = 1; i <= totalPages; i++) {
						if (i == pageNum) {
				%>
				<option selected="selected"><%=i%></option>
				<%
					} else {
				%>
				<option><%=i%></option>
				<%
					}
					}
				%>
			</select>
			页
		</div>
	</body>
</html>
