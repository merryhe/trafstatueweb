<%@ page language="java" pageEncoding="utf-8"%>
<%@ page contentType="text/html; charset=utf-8" language="java"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<link href="res/niceforms-default.css" rel="stylesheet" type="text/css" />
	<head>
		<meta charset="utf-8">
		<title>Insert title here</title>

		<script type="text/javascript">

			function pic_chg()
			{
				<!-- 警员编号判断 -->
				if (document.form1.jybh.value=="" || document.form1.jybh.value==null) {
					alert("警员编号不能为空！");
					return false;
				}
				<!-- 用户姓名判断 -->
				if (document.form1.yhxm.value=="" || document.form1.yhxm.value==null) {
					alert("用户姓名不能为空！");
					return false;
				}
				<!-- 终端号码判断 -->
				if (document.form1.zdhm.value=="" || document.form1.zdhm.value==null) {
					alert("终端号码不能为空！");
					return false;
				}
				<!-- 登陆密码判断 -->
				if (document.form1.dlmm.value=="" || document.form1.dlmm.value==null) {
					alert("登陆密码不能为空！");
					return false;
				}
				<!-- 所属区域判断 -->
				if (document.form1.ssqy.value=="" || document.form1.ssqy.value==null) {
					alert("所属区域不能为空！");
					return false;
				}
				<!-- 申请成为管理员判断 -->
				if (document.form1.sqgly.value=="" || document.form1.sqgly.value==null) {
					sqgly = "1";
				} else {
					sqgly = "0";
				}
				window.location.href='UserServlet?method='+"add"+'&jybh='+document.getElementById('jybh').value+'&yhxm='+document.getElementById('yhxm').value+'&dlmm='+document.getElementById('dlmm').value+
					'&ssqy='+document.getElementById('ssqy').value+'&zdhm='+document.getElementById('zdhm').value+'&sqgly='+document.getElementById('sqgly').value
			}
		</script>
	</head>
	<body bgcolor="#EEF2FB">
		<table width="100%" height="31" border="0" cellpadding="0"
			cellspacing="0" class="left_topbg" id="table2">
			<tr>
				<td height="31">
					<div>
						添加用户
					</div>
				</td>
			</tr>
		</table>
		<div align="center">
			<form action="" method="post" id="form1" name="form1" class="niceform">
				<fieldset>
					<dl>
						<dt>
							<label for="jybh">
								警员编号
							</label>
						</dt>
						<dd>
							<input type="text" name="jybh" id="jybh" size="54" />
						</dd>
					</dl>
					<dl>
						<dt>
							<label for="yhxm">
								用户姓名
							</label>
						</dt>
						<dd>
							<input type="text" name="yhxm" id="yhxm" size="54" />
						</dd>
					</dl>
					<dl>
						<dt>
							<label for="zdhm">
								终端号码
							</label>
						</dt>
						<dd>
							<input type="text" name="zdhm" id="zdhm" size="54" />
						</dd>
					</dl>
					<dl>
						<dt>
							<label for="dlmm">
								登陆密码
							</label>
						</dt>
						<dd>
							<input type="text" name="dlmm" id="dlmm" size="54" />
						</dd>
					</dl>
					<dl>
						<dt>
							<label for="ssqy">
								所属区域
							</label>
						</dt>
						<dd>
							<input type="text" name="ssqy" id="ssqy" size="54" />
						</dd>
					</dl>
					<dl>
						<dt>
							<input type="checkbox" id="sqgly" name="sqgly" />
						</dt>
						<dd>
							<label for="sqgly">
								申请成为管理员
							</label>
						</dd>
					</dl>
					<dl class="register">
						<input type="button" onclick="pic_chg();" name="register" id="register" value="提    交" />
					</dl>
				</fieldset>
			</form>
		</div>
	</body>
</html>